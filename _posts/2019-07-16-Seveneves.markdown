---
layout: post
title:  "Book Review: Sevenevs"
date:   2019-07-16 12:12:53 -0500
categories: review book
---
Intriguing first section, suspenseful second section, but falls flat on third section. Clearly the third section needed more work but more importantly Neal Stephenson's writing style leaves somethign to be desired in that section